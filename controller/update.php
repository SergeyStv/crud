<?php

require_once __DIR__.'/../model/Article.php';
$article = new Article();

if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {
    $article->update($_GET['id'], $_POST['name'], $_POST['description'], $_POST['created_at']);

    header('Location: ../index.php');
}

if (isset($_GET['id'])) {
    $article = $article->findById($_GET['id']);
} else {
    header('Location: ../index.php');
}
require_once __DIR__ . '/../template/view.php';
