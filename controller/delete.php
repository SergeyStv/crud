<?php
require_once __DIR__ . '/../model/Article.php';
if (isset($_GET['id'])) {
    (new Article())->deleteById($_GET['id']);
}
header('Location: index.php');
