<?php
require_once __DIR__ . '/../model/Article.php';
if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {
    (new Article())->inserts($_POST['name'], $_POST['description'], $_POST['created_at']);
    header('Location: index.php');
}
$article['name'] = $article['description'] = '';
require_once __DIR__ . '/../template/view.php';
